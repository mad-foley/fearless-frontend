//card template for conferences
function createCard(name, description, conventionCenter, pictureUrl, starts, ends){
       return `
        <div class="shadow p-3 mb-5 bg-white rounded">
        <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${conventionCenter}</div>
            <p class="card-text p-2">${description}</p>
        </div>
        <div class="card-footer bg-lightgrey border-success">${getDate(starts)}-${getDate(ends)}</div>
        </div>
    `;

    }

function getDate(date) {
    return date.toLocaleDateString();
}

//need async for await
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    // will return promise unless we add an await
    try {
        const response = await fetch(url);

        if (!response.ok) {
          // Figure out what to do when the response is bad
          alert("Error!")
        } else {
          const data = await response.json();
          let times = 1;
          for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const title = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const starts = new Date(details.conference.starts);
              const ends = new Date(details.conference.ends);
              const conventionCenter = details.conference.location.name;
              const html = createCard(title, description, conventionCenter,pictureUrl, starts, ends);
              if(times > 3){
                times = 1
              }
              const column = document.querySelector(`.col-num${times}`);
              column.innerHTML += html;
              times += 1;


            }
          }
        }
        
    } catch (e) {
        alert("Error - Check Log")
        console.log(e)
    }

});
