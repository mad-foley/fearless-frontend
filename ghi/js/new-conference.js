window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const selection = document.getElementById('location');
      for(let location of data.locations){
        let option = document.createElement('option');
        option.value = location.name;
        option.innerHTML = location.name;
        selection.appendChild(option)
      }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const readable = JSON.parse(json);
        const newjson = JSON.stringify(readable)
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: newjson,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const confResponse = await fetch(conferenceUrl, fetchConfig);
        if (confResponse.ok) {
            formTag.reset();
            const newConference = await confResponse.json;
            console.log(newConference)
        }
    })
    }

});
