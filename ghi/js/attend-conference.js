window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      };
      const loader = document.getElementById('loading-conference-spinner')
      selectTag.classList.remove('d-none')
      loader.classList.add('d-none')
    }
    const formTag = document.getElementById('create-attendee-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const readable = Object.fromEntries(formData);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(readable.conference)
        const newAttendeeUrl = `http://localhost:8001${readable.conference}attendees/`;
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const attendeeResponse = await fetch(newAttendeeUrl, fetchConfig);
        if (attendeeResponse.ok) {
            const sucess = document.getElementById("success-message");
            sucess.classList.remove('d-none');
            formTag.classList.add('d-none')
            const newAttendee = await attendeeResponse.json;
        }
    });
});
