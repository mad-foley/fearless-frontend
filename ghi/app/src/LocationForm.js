import React, { useEffect, useState } from 'react';

function LocationForm () {

  const [states, setStates] = useState([]);
  const fetchData = async () => {
      const url = 'http://localhost:8000/api/states/';

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setStates(data.states)
      } else{
          console.log('error')
      }
    }


  useEffect(() => {

    fetchData();
  }, []);




  const handleInputChange = async (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setMyUpdates({...myUpdates, [name]: value})
  }


  const [myUpdates, setMyUpdates] = useState({});

  const handleSubmit = async (event) => {
    event.preventDefault();
    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(myUpdates),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationUrl, fetchConfig);

    if (response.ok) {
      const newLocation = await response.json();
      console.log(newLocation)
     } else {
      console.log('error')
     }
}

return (
      <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input placeholder="Name" required type="text" name="name" onChange={handleInputChange} id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Room count" required type="number" onChange={handleInputChange} name="room_count" id="room_count" className="form-control"/>
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="City" required type="text" name="city" onChange={handleInputChange} id="city" className="form-control"/>
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
              <select required name="state" onChange={handleInputChange} id="state" className="form-select">
                  <option value="">Choose a state</option>
                  {states.map(state => {
                      return (
                          <option key={state.abbreviation} value={state.abbreviation}>
                            {state.name}
                          </option>
                      );
                      })}
              </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
                    }

export default LocationForm
